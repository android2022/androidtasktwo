package by.dazerty.androidtasktwo

import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.transition.TransitionInflater
import by.dazerty.androidtasktwo.itemrecyclerview.Item
import by.dazerty.androidtasktwo.itemrecyclerview.ItemListViewModel

private const val ARG_ITEM_ID = "item_id"

/**
 * детальная вью для элемента списка
 */
class ItemDetailFragment : Fragment() {
    private var itemId: Int? = null
    private var item : Item? = null
    private lateinit var titleTextView: TextView
    private lateinit var descriptionTextView: TextView
    private lateinit var imageView: ImageView

    private lateinit var itemListViewModel : ItemListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition = TransitionInflater.from(context)
                .inflateTransition(R.transition.simple_fragment_transition)

            sharedElementReturnTransition = TransitionInflater.from(context)
                .inflateTransition(android.R.transition.no_transition)
        }

        itemListViewModel = ItemListViewModel()

        arguments?.let {
            itemId = it.getInt(ARG_ITEM_ID)

        }
        item = itemListViewModel.getItemByPosition(itemId)

//        говорю, что у меня есть свои пункты меню
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_item_detail, container, false)

        titleTextView = view.findViewById(R.id.title)
        descriptionTextView = view.findViewById(R.id.description)
        imageView = view.findViewById(R.id.imageView)

        val exitButton = view.findViewById<Button>(R.id.exit_button)
        exitButton.setOnClickListener {
//            exitProcess(1)
            (context as AppCompatActivity).finish()
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        titleTextView.text = item?.title
        descriptionTextView.text = item?.description

        //одинаковое transitionName на обоих фрагментах
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.transitionName = "imageViewTransitionName_" + item?.id
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        //закидываю свои меню итемы в главное меню
        inflater.inflate(R.menu.fragment_item_detail, menu)
    }

    //действия меню
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.return_back -> {
                (context as AppCompatActivity).onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(itemId: Int) =
            ItemDetailFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_ITEM_ID, itemId)
                }
            }
    }
}
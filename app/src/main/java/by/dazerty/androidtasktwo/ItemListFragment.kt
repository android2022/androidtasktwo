package by.dazerty.androidtasktwo

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import by.dazerty.androidtasktwo.itemrecyclerview.Item
import by.dazerty.androidtasktwo.itemrecyclerview.ItemListViewModel

//экран со списком элементов
class ItemListFragment : Fragment() {

    interface Callbacks {
        fun onItemSelected(itemId: Int, imageView: View)
    }

    private lateinit var itemListViewModel : ItemListViewModel

    //текущие коллбеки
    private var callbacks : Callbacks? = null
    //лист криминалов
    private lateinit var itemRecyclerView : RecyclerView
    //адаптер для генерации элеменов, 1000 элементов с тайтл и декср по порядку
    private lateinit var adapter : ItemAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        itemListViewModel = ItemListViewModel()

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            sharedElementEnterTransition = TransitionInflater.from(context)
//                .inflateTransition(R.transition.simple_fragment_transition)
//
//            sharedElementReturnTransition = TransitionInflater.from(context)
//                .inflateTransition(R.transition.simple_fragment_transition)
//        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callbacks = context as Callbacks?
    }

    override fun onDetach() {
        super.onDetach()
        callbacks = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_item_list, container, false)
        itemRecyclerView = view.findViewById(R.id.item_list)

        adapter = ItemAdapter(itemListViewModel.getItems())
        itemRecyclerView.adapter = adapter

        return view
    }

    companion object {
        fun newInstance(): ItemListFragment {
            return ItemListFragment()
        }
    }

    private inner class ItemAdapter(var items : List<Item>): RecyclerView.Adapter<ItemHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
            val view = layoutInflater.inflate(R.layout.fragment_item, parent, false)

            return  ItemHolder(view)
        }

        override fun onBindViewHolder(holder: ItemHolder, position: Int) {
            val item = items[position]
            holder.bind(item)
        }

        override fun getItemCount(): Int {
            return items.size
        }
    }

    //холдер хранит представление для ячейки ресайклера
    private inner class ItemHolder(view : View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private lateinit var item : Item
        //получили контролы
        val titleTextView : TextView = itemView.findViewById(R.id.title)
        val descriptionTextView : TextView = itemView.findViewById(R.id.description)
        val imageView : ImageView = itemView.findViewById(R.id.imageView)

        init {
            itemView.setOnClickListener(this)
        }

        //привязка данных к вью
        fun bind(item: Item) {
            this.item = item
            titleTextView.text = item.title
            descriptionTextView.text = item.description

            //одинаковые transitionName
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imageView.transitionName = getString(R.string.simple_fragment_transition, item.id)
            }
        }

        //кликабл интерфейс для всей панели
        override fun onClick(view : View) {
            callbacks?.onItemSelected(item.id, imageView)
        }
    }
}
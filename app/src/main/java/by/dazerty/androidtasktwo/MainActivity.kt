package by.dazerty.androidtasktwo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat

const val ITEM_DETAIL_VIEW = "ITEM_DETAIL_VIEW"

class MainActivity : AppCompatActivity(), ItemListFragment.Callbacks {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //поиск фрагмента
        val currentFragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
        //если еще нет, то создаем и коммитим в менеджер
        if (currentFragment == null) {
            val fragment = ItemListFragment.newInstance()
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, fragment)
                .commit()
        }
    }

    override fun onItemSelected(itemId: Int, imageView: View) {
        val fragment = ItemDetailFragment.newInstance(itemId)
        var transitionName = ViewCompat.getTransitionName(imageView)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.apply {
            if (transitionName != null)
                addSharedElement(imageView, transitionName)
            addToBackStack(ITEM_DETAIL_VIEW)//чтоб возвращаться назад
            replace(R.id.fragment_container, fragment, ITEM_DETAIL_VIEW)
            commit()
        }
    }
}
package by.dazerty.androidtasktwo.itemrecyclerview

data class Item(var id: Int, var title: String, var description: String)
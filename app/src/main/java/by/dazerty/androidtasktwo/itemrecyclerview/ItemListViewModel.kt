package by.dazerty.androidtasktwo.itemrecyclerview

import androidx.lifecycle.ViewModel

class ItemListViewModel : ViewModel() {
    private val items = MutableList(1000)
        { i -> Item(i + 1, "Title ${i + 1}", "Description ${i + 1}") }

    fun getItemByPosition(i: Int?) : Item? {
        if (i == null) return null

        return items[i - 1]
    }

    fun getItems() : MutableList<Item> {
        return items
    }
}